Rails.application.routes.draw do
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
  post 'register', to: 'users#register'
  put 'redeem/:id', to: 'points#redeem'
  resources :points
  resources :users
end
