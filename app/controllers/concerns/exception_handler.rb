module ExceptionHandler

  # agrega el método "include"
  extend ActiveSupport::Concern

  # define subclases de errores personalizados - 'rescue' atrapa 'StandardErrors'
  class AuthenticationError < StandardError; end
  class MissingToken < StandardError; end
  class InvalidToken < StandardError; end
  class ExpiredSignature < StandardError; end

  included do
    # define manejadores personalizados
    rescue_from ActiveRecord::RecordNotFound, with: :four_zero_four
    rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two
    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
    rescue_from ExceptionHandler::MissingToken, with: :four_twenty_two
    rescue_from ExceptionHandler::InvalidToken, with: :four_twenty_two
    rescue_from ExceptionHandler::ExpiredSignature, with: :four_ninety_eight

  end

  private

  # respuesta en JSON con mensaje y código 404 - Not found
  def four_zero_four(e)
    json_response({ message: Message.not_found(get_resource_name_from_error(e.to_s)) }, :not_found)
  end

  # respuesta en JSON con mensaje y código 422 - Unprocessable entity
  def four_twenty_two(e)
    json_response({ message: e.message }, :unprocessable_entity)
  end

  # respuesta en JSON con mensaje y código 401 - Unauthorized
  def unauthorized_request(e)
    json_response({ message: e.message }, :unauthorized)
  end

  # respuesta en JSON con mensaje y código 498 - Invalid token
  def four_ninety_eight(e)
    json_response({ message: "Token expirado. Inicie sesión nuevamente.", token_valido: false }, :invalid_token)
  end

  # obtiene nombre del recurso a partir del string del error generado
  # el error en inglés siempre es, por ejemplo: "Couldn't find Empresa with 'id'=10"
  # nos interesa conseguir el substring "Empresa" para incrustar en Message.not_found
  def get_resource_name_from_error(string)
    find_pos_start = string.index('find')
    with_pos_start = string.index('with')

    find_pos_end = find_pos_start + 5
    with_pos_without_space = with_pos_start - 2

    string[find_pos_end..with_pos_without_space]
  end

end
