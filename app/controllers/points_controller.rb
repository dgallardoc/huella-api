class PointsController < ApplicationController
  before_action :set_points, only: [:show, :update, :destroy, :redeem]


  def index
    @points = point.all.order(id: :desc)
    json_response(@points)
  end

  def create
    @point = Point.create!(point_params)
    json_response(@point, :created)
  end

  def show
    json_response(@point)
  end

  def update
    save
    @point.update(point_params)
    json_response(@point, :ok)
  end

  def redeem
    reset
    @point.update(point_params)
    json_response(@point, :ok)
  end

  def destroy
    @point.destroy
    json_response({ :eliminado => true }, :ok)
  end

  private

  def point_params
    params.permit(
      :id, :totalpoints, :user, :reset
    )
  end

  def reset
    @left = point_params[:totalpoints].to_i - point_params[:reset].to_i
    params[:totalpoints] = @left
    # h = {'id' => params[:id], 'totalpoints' => @left}
    # puts h
  end

  def save
    @a1 = point_params[:totalpoints].to_i
    @a2 = @point.totalpoints.to_i
    puts @a1
    puts @a2
    @add = @a1 + @a2
    params[:totalpoints] = @add

  end

  def set_points
    @point = Point.find(params[:id])
  end
end
