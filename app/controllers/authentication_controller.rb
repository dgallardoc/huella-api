class AuthenticationController < ApplicationController
  skip_before_action :authorize_request, only: :authenticate
  # return auth token once user is authenticated
 def authenticate
   userid
   auth_token =
     AuthenticateUser.new(auth_params[:email], auth_params[:password]).call
   json_response(auth: {auth_token: auth_token, id: @user.id, name: @user.name, footprint: @user.footprint})
 end

 private

 def userid
    @user = User.find_by(email: auth_params[:email])
 end

 def auth_params
   params.permit(:email, :password)
 end
end
