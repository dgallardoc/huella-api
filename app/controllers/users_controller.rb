class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  skip_before_action :authorize_request, only: [:create, :register]
  # POST /signup
  # return authenticated token upon signup
  def create
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password_digest).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

  def show
    json_response(@user)
  end

  def update
    @user.update(user_params)
    json_response(@user, :ok)
  end

  def destroy
    @user.destroy
    json_response({ :eliminado => true }, :ok)
  end

  # def register
  #   @pwd = user_params[:password_digest]
  #   @pwdC =user_params[:password_confirmation]
  #   if @pwd != @pwdC
  #     json_response('contraseñas no coinciden')
  #   else
  #     user = User.create!(newUser)
  #     auth_token = AuthenticateUser.new(user.email, user.password_digest).call
  #     response = { message: Message.account_created, auth_token: auth_token }
  #     json_response(user, :created)
  #   end
  # end

  private

  def user_params
    params.permit(
      :id,
      :name,
      :email,
      :password_digest,
      :footprint
    )
  end
  # def newUser
  #   params.permit(
  #     :name,
  #     :email,
  #     :password_digest,
  #     :role,
  #     :footprint
  #   )
  # end

  def set_user
    @user = User.find(params[:id])
  end
end
