class User < ApplicationRecord
  after_create :init_points
  has_one :point
  # has_secure_password
  validates_presence_of :name, :email, :password_digest

  def init_points
    points = Point.create!(user_id: self.id, totalpoints: 0, updated_at: Date.yesterday)
  end
end
