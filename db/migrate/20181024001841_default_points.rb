class DefaultPoints < ActiveRecord::Migration[5.1]
  def change
    change_column_default(:points, :totalpoints, 0)
  end
end
